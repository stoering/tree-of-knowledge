﻿using UnityEngine;
using UnityEditor;
using Core;

[CustomEditor(typeof(Planet))]
public class PlanetEditor : Editor {

    Planet planet;
    Editor shapeEditor;
    Editor colourEditor;

	public override void OnInspectorGUI()
	{
        DrawDefaultInspector();
        DrawSettingsEditor(this.planet.PlanetLodSettings, ref this.planet.LodSettingsFoldout, ref this.shapeEditor);
        DrawSettingsEditor(this.planet.PlanetNoiseSettings, ref this.planet.NoiseSettingsFoldout, ref this.shapeEditor);
        DrawSettingsEditor(this.planet.PlanetColorSettings, ref this.planet.ColorSettingsFoldout, ref this.colourEditor);
	}

    void DrawSettingsEditor(Object settings, ref bool foldout, ref Editor editor)
    {
        if (settings != null)
        {
            foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (foldout)
                {
                    CreateCachedEditor(settings, null, ref editor);
                    editor.OnInspectorGUI();
                }
            }
        }
    }

	private void OnEnable()
	{
        this.planet = (Planet)this.target;
	}
}
