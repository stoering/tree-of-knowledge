﻿/*
// Here's a singleton class to use if I decide to make more of these.
public sealed class ModInterface
{
    private static readonly Lazy<ModInterface> lazy = new Lazy<ModInterface>(() => new ModInterface());
    private ModInterface() {}

    public static ModInterface Me { get { return lazy.Value; } }
}
*/
using System;
using MoonSharp.Interpreter;
using MoonSharp.VsCodeDebugger;

namespace Interface
{
    public sealed class ModInterface
    {
        private static readonly Lazy<ModInterface> lazy = new Lazy<ModInterface>(() => new ModInterface());
        private ModInterface() { }

        public static ModInterface Me { get { return lazy.Value; } }


        private MoonSharpVsCodeDebugServer debugServer;
        public static MoonSharpVsCodeDebugServer DebugServer { get => Me.debugServer; }


        public static void ActivateDebugger()
        {
            // Create the debugger server
            Me.debugServer = new MoonSharpVsCodeDebugServer();
            // Start the debugger server
            Me.debugServer.Start();
        }

        public static void DoTestStuff()
        {
            MoonSharpFactorial();
        }

        private static double MoonSharpFactorial()
        {
            string script = @"    
		    -- defines a factorial function
		    function fact (n)
			    if (n == 0) then
				    return 1
			    else
				    return n*fact(n - 1)
			    end
		    end
            
    		return fact(5)";

            DynValue res = Script.RunString(script);
            return res.Number;
        }
    }
}