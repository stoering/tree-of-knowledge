﻿using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Expressions
{
	class LiteralExpression : Expression
	{
		DynValue m_Value;

		public DynValue Value
		{
			get { return this.m_Value; }
		}


		public LiteralExpression(ScriptLoadingContext lcontext, DynValue value)
			: base(lcontext)
		{
            this.m_Value = value;
		}


		public LiteralExpression(ScriptLoadingContext lcontext, Token t)
			: base(lcontext)
		{
			switch (t.Type)
			{
				case TokenType.Number:
				case TokenType.Number_Hex:
				case TokenType.Number_HexFloat:
                    this.m_Value = DynValue.NewNumber(t.GetNumberValue()).AsReadOnly();
					break;
				case TokenType.String:
				case TokenType.String_Long:
                    this.m_Value = DynValue.NewString(t.Text).AsReadOnly();
					break;
				case TokenType.True:
                    this.m_Value = DynValue.True;
					break;
				case TokenType.False:
                    this.m_Value = DynValue.False;
					break;
				case TokenType.Nil:
                    this.m_Value = DynValue.Nil;
					break;
				default:
					throw new InternalErrorException("type mismatch");
			}

			if (this.m_Value == null)
				throw new SyntaxErrorException(t, "unknown literal format near '{0}'", t.Text);

			lcontext.Lexer.Next();
		}

		public override void Compile(Execution.VM.ByteCode bc)
		{
			bc.Emit_Literal(this.m_Value);
		}

		public override DynValue Eval(ScriptExecutionContext context)
		{
			return this.m_Value;
		}
	}
}
