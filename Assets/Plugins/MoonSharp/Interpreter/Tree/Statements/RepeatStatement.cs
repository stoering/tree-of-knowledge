﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;


namespace MoonSharp.Interpreter.Tree.Statements
{
	class RepeatStatement : Statement
	{
		Expression m_Condition;
		Statement m_Block;
		RuntimeScopeBlock m_StackFrame;
		SourceRef m_Repeat, m_Until;

		public RepeatStatement(ScriptLoadingContext lcontext)
			: base(lcontext)
		{
            this.m_Repeat = CheckTokenType(lcontext, TokenType.Repeat).GetSourceRef();

			lcontext.Scope.PushBlock();
            this.m_Block = new CompositeStatement(lcontext);

			Token until = CheckTokenType(lcontext, TokenType.Until);

            this.m_Condition = Expression.Expr(lcontext);

            this.m_Until = until.GetSourceRefUpTo(lcontext.Lexer.Current);

            this.m_StackFrame = lcontext.Scope.PopBlock();
			lcontext.Source.Refs.Add(this.m_Repeat);
			lcontext.Source.Refs.Add(this.m_Until);
		}

		public override void Compile(ByteCode bc)
		{
			Loop L = new Loop()
			{
				Scope = m_StackFrame
			};

			bc.PushSourceRef(this.m_Repeat);

			bc.LoopTracker.Loops.Push(L);

			int start = bc.GetJumpPointForNextInstruction();

			bc.Emit_Enter(this.m_StackFrame);
            this.m_Block.Compile(bc);

			bc.PopSourceRef();
			bc.PushSourceRef(this.m_Until);
			bc.Emit_Debug("..end");

            this.m_Condition.Compile(bc);
			bc.Emit_Leave(this.m_StackFrame);
			bc.Emit_Jump(OpCode.Jf, start);

			bc.LoopTracker.Loops.Pop();

			int exitpoint = bc.GetJumpPointForNextInstruction();

			foreach (Instruction i in L.BreakJumps)
				i.NumVal = exitpoint;

			bc.PopSourceRef();
		}


	}
}
