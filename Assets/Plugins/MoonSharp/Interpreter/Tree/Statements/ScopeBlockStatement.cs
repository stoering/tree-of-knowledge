﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Statements
{
	class ScopeBlockStatement : Statement
	{
		Statement m_Block;
		RuntimeScopeBlock m_StackFrame;
		SourceRef m_Do, m_End;

		public ScopeBlockStatement(ScriptLoadingContext lcontext)
			: base(lcontext)
		{
			lcontext.Scope.PushBlock();

            this.m_Do = CheckTokenType(lcontext, TokenType.Do).GetSourceRef();

            this.m_Block = new CompositeStatement(lcontext);

            this.m_End = CheckTokenType(lcontext, TokenType.End).GetSourceRef();

            this.m_StackFrame = lcontext.Scope.PopBlock();
			lcontext.Source.Refs.Add(this.m_Do);
			lcontext.Source.Refs.Add(this.m_End);
		}



		public override void Compile(Execution.VM.ByteCode bc)
		{
			using(bc.EnterSource(this.m_Do))
				bc.Emit_Enter(this.m_StackFrame);

            this.m_Block.Compile(bc);

			using (bc.EnterSource(this.m_End))
				bc.Emit_Leave(this.m_StackFrame);
		}

	}
}
