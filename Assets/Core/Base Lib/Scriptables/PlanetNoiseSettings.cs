﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BaseLib
{
    [Serializable]
    public class NoiseSettings
    {
        public bool enabled;
        public bool addToMask;
        public bool useMask;

        [Range(1,8)] public int numLayers = 1;
        public float strength = 1;
        public float baseRoughness = 1;
        public float roughness = 2;
        [Range(0.001f, 1.5f)] public float octivePersistance = 0.5f;
    }

    [CreateAssetMenu()]
    public class PlanetNoiseSettings : ScriptableObject
    {
        public int baseSeed = 0;
        [Range(-100,100)] public float ScaledOceanHeight = -1;

        public List<NoiseSettings> NoiseLayer = new List<NoiseSettings>();
    }
}
