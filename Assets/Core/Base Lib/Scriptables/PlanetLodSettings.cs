﻿using UnityEngine;

namespace BaseLib
{
    [CreateAssetMenu()]
    public class PlanetLodSettings : ScriptableObject
    {
        public int PlanetRadius = 500;
        [Range(2, 256)]
        public int ChunkResolution = 16;
        [Range(0, 16)]
        public int MinLodDepth = 0;
        [Range(0, 16)]
        public int MaxLodDepth = 4;
    }
}
