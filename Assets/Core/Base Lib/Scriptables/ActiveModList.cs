﻿using System;
using UnityEngine;

namespace BaseLib
{
    [Serializable]
    public struct ModDescriptor
    {
        public string Name;
        public string File;
    }

    [Serializable]
    public class ActiveModList : ScriptableObject
    {
        public ModDescriptor NoiseGenerator;
    }
}
