﻿using System;

namespace Core
{
    public class ChunkDataMismatchException : Exception
    {
        public ChunkDataMismatchException(string s) : base(s) { }
    }

    public class ChunkCreationFailureException : Exception
    {
        public ChunkCreationFailureException(string s) : base(s) { }
    }

    public class SaveFileException : Exception
    {
        public SaveFileException(string s) : base(s) { }
    }

    public class LoadFileException : Exception
    {
        public LoadFileException(string s) : base(s) { }
    }
}
