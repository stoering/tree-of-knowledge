﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Core
{
    public class TechnologyManager : MonoBehaviour
    {
        /// <summary>
        /// Base class for all serializable tech trees.
        /// </summary>
        [System.Serializable]
        public class TechTreeNode
        {
            public string RootName = "";
            public string SurfaceName = "";
            public string Description = "";
            public ushort RootCost = 0;
            public ushort SurfaceCost = 0;
        }

        /// <summary>
        /// TechTreeMasterNode describes a node of the default tech tree. This tree may be 
        /// changed via mods or dev mode, but it will never be changed during default gameplay.
        /// This is built from the flat trees, but all game files are derived from this tree.
        /// </summary>
        [System.Serializable]
        private class TechTreeMasterNode : TechTreeNode
        {
            public List<ushort> Prerequesites = new List<ushort>();
            public List<ushort> Dependencies = new List<ushort>();
        }

        // Used in the Master Tree 
        [System.Serializable]
        private class TreeDescriptor
        {
            public string Namespace = "Mod";
            public ushort NodeCount = 0;
            public bool TreeActive = false;
        }

        /// <summary>
        /// Wrapper needed by Unity to write the tech tree list to Json.
        /// </summary>
        [System.Serializable]
        private class TechTreeMasterTree
        {
            public List<TreeDescriptor> IncludedTrees = new List<TreeDescriptor>();
            public List<TechTreeMasterNode> Tree = new List<TechTreeMasterNode>();
        }

        /// <summary>
        /// TechTreeFlatNode describes a node for the plaintext tech tree. Instead of using UUIDS,
        /// it uses string literals. Much slower to manipulate, but less prone to human error. Can be namespaced.
        /// </summary>
        [System.Serializable]
        public class TechTreeFlatNode : TechTreeNode
        {
            // The namespace of the node. Multiple trees can be combined in one file this way.
            public string Namespace = "";
            // The namespace of the tree file followed by the root name. This MUST match both the namespace
            // and the RootName in order for it to be properly saved/written. Example: C_math_Trigonometery
            public List<string> Prerequesites = new List<string>();
            public List<string> Dependencies = new List<string>();
        }

        [System.Serializable]
        public class TechTreeFlatTree
        {
            public string Namespace = "Mod_X";
            public bool TreeActive = true;
            public List<TechTreeFlatNode> Tree = new List<TechTreeFlatNode>();
        }

        // Contains all trees in all namespaces. May be regenerated from the individual saves.
        // Cannot be converted to flat tree if there are duplicates due to the loss of namespace.
        private readonly TechTreeMasterTree MasterTree = new TechTreeMasterTree();

        [SerializeField]
        private List<TechTreeFlatTree> flatTrees = new List<TechTreeFlatTree>();

        public List<TechTreeFlatTree> FlatTrees { get => this.flatTrees; set => this.flatTrees = value; }

        // Start is called before the first frame update
        void Start()
        {
            DirectoryInfo directory = new DirectoryInfo(Application.dataPath + "/RuntimeAssets");
            FileInfo[] flatTreeFiles = directory.GetFiles("*.txtTree");

            foreach (FileInfo flatTreeFile in flatTreeFiles)
            {
                // Load our flat tree count.
                this.FlatTrees.Add(LoadFlat(flatTreeFile.FullName));
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnMouseDown()
        {
            foreach (TechTreeFlatTree flatTree in this.FlatTrees)
            {
                SaveFlat(flatTree, flatTree.Namespace + ".txtTree");
            }
            RegenerateMaster();
            SaveMaster();
        }

        /// <summary>
        /// Uses string comparisons to regenerate the master tree from a list of flat trees.
        /// </summary>
        void RegenerateMaster()
        {
            List<TechTreeFlatNode> masterFlatTree = this.FlatTrees.Where(x => x.TreeActive).SelectMany(x => x.Tree).ToList();

            masterFlatTree.Add(new TechTreeFlatNode()
            {
                RootName = "FINAL"
            });
            masterFlatTree.Insert(0, new TechTreeFlatNode()
            {
                RootName = "INITIAL"
            });

            // Copy just the common parameters. We need the list to exist before we can populate the UUIDs.
            this.MasterTree.Tree = masterFlatTree.Select(x => new TechTreeMasterNode()
            {
                RootName = x.RootName,
                SurfaceName = x.SurfaceName,
                Description = x.Description,
                RootCost = x.RootCost,
                SurfaceCost = x.SurfaceCost
            }).ToList();

            // Yes, this takes a while, but we only have to do it once. Reference each node by its position in the master tree.
            for (int curNode = 1; curNode < this.MasterTree.Tree.Count - 1; curNode++)
            {
                foreach (string Prereq in masterFlatTree[curNode].Prerequesites)
                    this.MasterTree.Tree[curNode].Prerequesites.Add((ushort)masterFlatTree.FindIndex(a => Prereq.Equals(a.Namespace + "_" + a.RootName)));
                foreach (string Dependency in masterFlatTree[curNode].Dependencies)
                    this.MasterTree.Tree[curNode].Dependencies.Add((ushort)masterFlatTree.FindIndex(a => Dependency.Equals(a.Namespace + "_" + a.RootName)));

                if (this.MasterTree.Tree[curNode].Prerequesites.Count == 0)
                {
                    // Form a new link between this and Node 0. This allows the tree to be dynamically rendered.
                    this.MasterTree.Tree[curNode].Prerequesites.Add(0);
                    this.MasterTree.Tree[0].Dependencies.Add((ushort)curNode);
                }
                if (this.MasterTree.Tree[curNode].Dependencies.Count == 0)
                {
                    // Form a new link between this and the final node. This allows the tree to be dynamically rendered.
                    this.MasterTree.Tree[curNode].Dependencies.Add((ushort)(this.MasterTree.Tree.Count - 1));
                    this.MasterTree.Tree[this.MasterTree.Tree.Count - 1].Prerequesites.Add((ushort)curNode);
                }
            }

            SaveMaster();
        }

        /// <summary>
        /// Saves the master tree to the default location.
        /// </summary>
        public void SaveMaster()
        {
            SaveString(JsonUtility.ToJson(this.MasterTree, true), "SkillTree.tree");
        }

        /// <summary>
        /// Save a flat tree to the specified file.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="fileName"></param>
        private void SaveFlat(TechTreeFlatTree tree, string fileName)
        {
            SaveString(JsonUtility.ToJson(tree, true), fileName);
        }

        /// <summary>
        /// Saves a string to a file on Application.dataPath.
        /// </summary>
        /// <param name="toSave"></param>
        /// <param name="fileName"></param>
        private void SaveString(string toSave, string fileName)
        {
            string dataPath = Path.Combine(Application.dataPath, "RuntimeAssets", fileName);

            using (StreamWriter streamWriter = File.CreateText(dataPath))
            {
                streamWriter.Write(toSave);
            }
        }

        /// <summary>
        /// Loads the master tree from the default location.
        /// </summary>
        public void LoadMaster()
        {
            JsonUtility.FromJsonOverwrite(LoadJSONString("Temp/SkillTree.tree"), this.MasterTree);
        }

        /// <summary>
        /// Pulls a flat tree from a file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private TechTreeFlatTree LoadFlat(string fileName)
        {
            string s = LoadJSONString(fileName);
            return JsonUtility.FromJson<TechTreeFlatTree>(s);
        }

        /// <summary>
        /// Loads a Json script from a specified file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>Json string</returns>
        private string LoadJSONString(string fileName)
        {
            string dataPath = Path.Combine(Application.dataPath, "RuntimeAssets", fileName);

            Debug.Log(dataPath);

            using (StreamReader streamReader = File.OpenText(dataPath))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
