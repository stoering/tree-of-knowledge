﻿using UnityEngine;

namespace Core
{
    /// <summary>
    /// Emables the camera to orbit any planet and zoom/rotate about it. Can be toggled on and off if multiple camera modes are needed.
    /// </summary>
    public class OrbitalCamera : MonoBehaviour
    {
        public enum OrbitStates
        {
            ActivelyDragging,
            Gliding,
            Stopped,
            Deactivated
        }

        // The target of the camera. The camera will always point to this object.
        public Transform Target;

        // The main orthographic camera.
        public Camera MainCamera;

        // The minimum and maximum zoom levels.
        [Range(0.00001f, 5f)] public float MinZoom = 0.1f;
        [Range(0.00001f, 5f)] public float MaxZoom = 2f;
        public float CurrentZoom = 0;

        // Control the speed of zooming and dezooming.
        [Range(0.0001f, 0.9999f)] public float ZoomStep = 0.01f;

        // Friction and stopping speed for glides.
        public float GlideBreakFriction = 0.25f;
        public float GlideFriction = 0.0f;
        public float ZoomFriction = 0.1f;
        public float MinGlideSpeed = 0.1f;
        public float AutoRotateSpeed = 0.1f;

        public OrbitStates OrbitState = OrbitStates.Stopped;

        private Vector3 MouseStartPos { get; set; }
        private Vector3 CurrentMousePos { get; set; }
        private Vector3 LastCrossProduct { get; set; }
        private float LastAngle { get; set; }

        private void Start()
        {
            Planet planet = this.Target.GetComponent<Planet>();
            int planetRadius = planet ? planet.PlanetLodSettings.PlanetRadius : 1000;

            if (this.MaxZoom <= this.MinZoom)
            {
                this.MaxZoom = 1;
            }

            this.MaxZoom *= planetRadius * 2;
            this.MinZoom *= planetRadius * 2;
            this.CurrentZoom = this.MainCamera.orthographicSize;
        }

        private void Update()
        {
            // Always zoom, unrelated to rotation.
            Zoom();

            float cameraRotation = -this.transform.eulerAngles.z; //(MainCamera.transform.localEulerAngles.z * (1 - this.AutoRotateSpeed)) - (transform.eulerAngles.z * this.AutoRotateSpeed);
            this.MainCamera.transform.localEulerAngles = new Vector3(0, 0, cameraRotation);

            // Check for click or release.
            if (Input.GetMouseButtonDown(0))
            {
                this.OrbitState = OrbitStates.ActivelyDragging;
                this.MouseStartPos = GetMouseHit();
            }
            if (Input.GetMouseButtonUp(0))
            {
                HandleDrop();
            }

            switch (this.OrbitState)
            {
                case OrbitStates.ActivelyDragging:
                    HandleDrag();
                    break;
                case OrbitStates.Gliding:
                    HandleGlide();
                    break;
                case OrbitStates.Stopped:
                case OrbitStates.Deactivated:
                // Do nothing.
                default:
                    break;
            }
        }

        /// <summary>
        /// Rotates the camera according to the delta mouse position.
        /// </summary>
        private void HandleDrag()
        {
            this.CurrentMousePos = GetMouseHit();
            if (this.OrbitState == OrbitStates.ActivelyDragging)
            {
                RotateCamera();
            }
        }

        /// <summary>
        /// Ends a drag event when the mouse has left the planet or the click was released.
        /// </summary>
        private void HandleDrop()
        {
            // Slow down the rotation a bit. The release feels faster than the actual drag most of the time.
            this.LastAngle *= (1 - this.GlideBreakFriction);

            this.OrbitState = OrbitStates.Gliding;
        }

        /// <summary>
        /// Continues to rotate the camera along the last used axis. Will eventually stop.
        /// </summary>
        private void HandleGlide()
        {
            this.LastAngle *= (1 - this.GlideFriction);

            if (this.LastAngle < (this.MinGlideSpeed * Time.deltaTime))
            {
                this.OrbitState = OrbitStates.Stopped;
            }
            else
            {
                this.transform.RotateAround(this.Target.position, this.LastCrossProduct, this.LastAngle);
            }
        }

        /// <summary>
        /// Rotates the camera around the given vectors.
        /// </summary>
        private void RotateCamera()
        {
            // in case the spehre model is not a perfect sphere..
            Vector3 dragEndPosition = this.CurrentMousePos.normalized;
            Vector3 dragStartPosition = this.MouseStartPos.normalized;
            // calc a vertical vector to rotate around..
            Vector3 cross = Vector3.Cross(dragEndPosition, dragStartPosition);
            // calc the angle for the rotation..
            float angle = Vector3.SignedAngle(dragEndPosition, dragStartPosition, cross);
            // roatate around the vector..
            this.transform.RotateAround(this.Target.position, cross, angle);

            this.LastCrossProduct = cross;
            this.LastAngle = angle;
        }

        /// <summary>
        /// Projects the mouse position to the sphere and returns the intersection point. 
        /// </summary>
        /// <returns></returns>
        private Vector3 GetMouseHit()
        {
            if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit))
            {
                HandleDrop();
            }
            return hit.point;
        }

        /// <summary>
        /// Grows or shrinks the camera box for an effective zoom.
        /// </summary>
        void Zoom()
        {
            float mouseScroll = Input.GetAxis("Mouse ScrollWheel");


            if ((mouseScroll < 0.0f) || (mouseScroll > 0.0f))
            {
                float cameraZoom = this.MainCamera.orthographicSize * (1 - (this.ZoomStep * mouseScroll));
                if (cameraZoom < this.MinZoom)
                {
                    cameraZoom = this.MinZoom;
                }
                if (cameraZoom > this.MaxZoom)
                {
                    cameraZoom = this.MaxZoom;
                }
                this.MainCamera.orthographicSize = cameraZoom;
                this.CurrentZoom = cameraZoom;

                // Slow the planet's rotation to keep the stress off of the LOD.
                this.LastAngle *= (1 - this.ZoomFriction);
            }
        }
    }
}
