﻿using UnityEngine;

namespace Core
{
    public class LodManager : MonoBehaviour
    {
        [SerializeField] private OrbitalCamera cameraScript;
        [SerializeField] private Planet planet;
        private int previousLod = 0;

        public OrbitalCamera CameraScript { get => this.cameraScript; set => this.cameraScript = value; }
        public Planet Planet { get => this.planet; set => this.planet = value; }

        // Update is called once per frame.
        void Update()
        {
            int lod = this.previousLod;
            float chunkSize = this.planet.SharedPlanetSettings.planetLodSettings.PlanetRadius * 2;

            // Get the (loose) chunk size from the planet size. More likely, there will be 2-3 chunks visible at a time.
            while (lod > 0)
            {
                chunkSize /= 2;
                lod--;
            }

            lod = this.previousLod;

            if (chunkSize > this.cameraScript.CurrentZoom)
            {
                lod++;
            }
            else if ((lod > 0) && (chunkSize * 2 < this.cameraScript.CurrentZoom))
            {
                lod--;
            }

            if (this.previousLod != lod && lod <= this.planet.PlanetLodSettings.MaxLodDepth)
            {
                this.previousLod = lod;
                this.Planet.SetLod(lod);
            }
        }
    }
}
