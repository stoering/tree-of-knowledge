﻿using UnityEngine;

namespace Core
{
    public class CameraRenderCollider : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private new BoxCollider collider;
        [SerializeField] private float screenToColliderRatio = 1;
        [SerializeField] private ViewState boxLodDepth = ViewState.Visible;

        public Camera MainCamera { get => this.mainCamera; set => this.mainCamera = value; }
        public BoxCollider Collider { get => this.collider; set => this.collider = value; }
        public float ScreenToColliderRatio { get => this.screenToColliderRatio; set => this.screenToColliderRatio = value; }
        public ViewState BoxLodDepth { get => this.boxLodDepth; set => this.boxLodDepth = value; }

        private void Start()
        {
            if (this.MainCamera == null)
            {
                this.MainCamera = Camera.main;
            }
            if (this.Collider == null)
            {
                this.Collider = GetComponent<BoxCollider>();
            }
        }

        // Update is called once per frame
        void Update()
        {
            float ColliderSize = this.MainCamera.orthographicSize * this.ScreenToColliderRatio * 2;

            this.Collider.size = new Vector3(ColliderSize * this.MainCamera.aspect, ColliderSize, this.Collider.size.z);
        }

        private void OnTriggerEnter(Collider other)
        {
            other.GetComponent<Chunk>()?.SetChunkEnterVisibility(this.BoxLodDepth);
        }

        private void OnTriggerExit(Collider other)
        {
            other.GetComponent<Chunk>()?.SetChunkExitVisibility(this.BoxLodDepth);
        }
    }
}
