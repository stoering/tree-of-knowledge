﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using UnityEngine;

namespace Core
{
    /// <summary>
    /// Creates DynGrids and maintains the free DynGrid pool. Also maintains the DynGridCreation thread.
    /// </summary>
    public sealed class GridFactory
    {
        // This class is a singleton. Use Me to get "this"
        private static readonly Lazy<GridFactory> lazy = new Lazy<GridFactory>(() => new GridFactory());
        private static GridFactory Me { get { return lazy.Value; } }

        // DynGrid Generation Thread:
        private Thread DynGridGeneratorAsync { get; set; }
        private static AutoResetEvent DynGridThreadWait { get; set; } = new AutoResetEvent(false);
        private bool DynGridGenerationThreadRunning { get; set; } = false;
        private bool DynGridGenerationThreadHalt { get; set; } = false;

        // Our DynGrids that can be used.
        private ConcurrentQueue<DynGrid> FreeDynGrids { get; set; } = new ConcurrentQueue<DynGrid>();
        private Transform FreeDynGridParent { get; set; } = null;

        // Internal structure used to generate DynGrids.
        private ConcurrentQueue<DynGridVoxelLoader> FreeDynGridData { get; set; } = new ConcurrentQueue<DynGridVoxelLoader>();
        // The urgent queue is of critical priority. The lazy queue is only generated when the fast queue is empty.
        private ConcurrentQueue<DynGridVoxelLoader> UrgentQueue { get; set; } = new ConcurrentQueue<DynGridVoxelLoader>();
        private ConcurrentQueue<DynGridVoxelLoader> LazyQueue { get; set; } = new ConcurrentQueue<DynGridVoxelLoader>();
        // When we have done all we can do on the second thread, we need to finish setting up the mesh data.
        private ConcurrentQueue<DynGridVoxelLoader> CallbackQueue { get; set; } = new ConcurrentQueue<DynGridVoxelLoader>();

        // Private and parameterless, guarantees that two can't be created.
        private GridFactory()
        {
            this.DynGridGenerationThreadRunning = true;
            this.DynGridGeneratorAsync = new Thread(RunDynGridGenerationThread);
            this.DynGridGeneratorAsync.Start();
        }

        /// <summary>
        /// Creates and returns a new dynamic grid. Initializes the grid to default values.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="urgent"></param>
        /// <returns></returns>
        public static DynGrid CreateDynGrid( Vector3 position, Vector2 size, bool urgent = false)
        {/*
            DynGrid DynGrid = CreateInactiveDynGrid(position);

            GenerateDynGridMeshes(DynGrid, urgent);

            //Return the DynGrid.
            return DynGrid;*/ return null;
        }

        /// <summary>
        /// Returns and initializes a DynGrid that is not currently being rendered. 
        /// </summary>
        /// <returns></returns>
        public static DynGrid CreateInactiveDynGrid( Vector3 position)
        {
            DynGrid DynGrid = GetFreeDynGrid();
            DynGrid.Initialize();

            return DynGrid;
        }

        public static void GenerateDynGridMeshes(DynGrid DynGrid, bool urgent = false)
        {/*
            if (Me.FreeDynGridData.TryDequeue(out DynGridVoxelLoader data) == false)
                data = new DynGridVoxelLoader();

            if (data == null) throw new ChunkCreationFailureException("Data dequeue fail");

            data.Init(true, true, true);
            // TODO: add data types to DynGrids when there are more than one. // data.Type = type;

            // Pass a function pointer from our DynGrid. The rest is automatic from here.
            data.Callback = DynGrid.SetMesh;

            // Add the DynGrid to the render queue. 
            if (urgent)
                Me.UrgentQueue.Enqueue(data);
            else
                Me.LazyQueue.Enqueue(data);
            DynGridThreadWait.Set();*/
        }

        /// <summary>
        /// Returns a DynGrid that is not currently being rendered. 
        /// </summary>
        /// <returns></returns>
        private static DynGrid GetFreeDynGrid()
        {
            if (Me.FreeDynGrids.TryDequeue(out DynGrid DynGrid) == false)
            {
                GameObject DynGridObject = new GameObject();
                DynGridObject.AddComponent<MeshFilter>().mesh = new Mesh();
                DynGridObject.AddComponent<MeshRenderer>();
                DynGridObject.AddComponent<MeshCollider>();
                DynGridObject.AddComponent<Rigidbody>().isKinematic = true;
                DynGrid = DynGridObject.AddComponent<DynGrid>();
            }

            return DynGrid;
        }

        /// <summary>
        /// Sends a DynGrid back to the free stack so we can reuse it later.
        /// Nests it under the returnDynGridParent object if one is specified.
        /// </summary>
        /// <param name="DynGrid"></param>
        public static void ReturnDynGrid(DynGrid DynGrid)
        {
            DynGrid.gameObject.SetActive(false);

            if (Me.FreeDynGridParent != null)
            {
                DynGrid.transform.parent = Me.FreeDynGridParent;
            }

            // Add the DynGrid to our queue.
            Me.FreeDynGrids.Enqueue(DynGrid);
        }

        /// <summary>
        /// Set the parent object for our DynGrids to nest under. If DynGrids are returned before
        /// this is provided, they will be left in their existing structure.
        /// </summary>
        /// <param name="transform"></param>
        public static void SetReturnDynGridParent(Transform transform)
        {
            Me.FreeDynGridParent = transform;
        }

        /// <summary>
        /// Take some time off the main thread to create DynGrids if they are available.
        /// </summary>
        public static void Tick()
        {
            // Check first before declaring anything.
            if (Me.CallbackQueue.TryDequeue(out DynGridVoxelLoader data))
            {
                do
                {
                    //data.Callback(data);
                    //data.Clear();
                    Me.FreeDynGridData.Enqueue(data);
                } while (Me.CallbackQueue.TryDequeue(out data));
            }
        }

        /// <summary>
        /// Emergency protocol halts the processing thread and moves all DynGrid loaders to the free queue.
        /// </summary>
        public static void DropAllProcessingDynGrids()
        {
            // Emergency freeze of our generation thread. Will finish the remaining DynGrid.
            DynGridThreadWait.Reset();
            Me.DynGridGenerationThreadHalt = true;
            DynGridVoxelLoader data;

            // Clean out all of the queues. Do not actually process any data.
            while (Me.UrgentQueue.TryDequeue(out data))
            {
                Me.FreeDynGridData.Enqueue(data);
            }
            while (Me.LazyQueue.TryDequeue(out data))
            {
                Me.FreeDynGridData.Enqueue(data);
            }
            // Call this one last to minimize the chance of a race condition leaving this queue with an item. Technically, the second thread
            // could access this after we get here. Hopefully, locks and the processing time for the threads above will ensure that we don't
            // ever leave the thread with a single invalid data DynGrid. We should handle the race anyways.
            while (Me.CallbackQueue.TryDequeue(out data))
            {
                Me.FreeDynGridData.Enqueue(data);
            }

            // Do not reenable the DynGrid generation thread. It will enable itself after a Set signal is sent.
        }

        /// <summary>
        /// Releases all unused grids and pending grid loaders for garbage collection.
        /// DO NOT USE THIS WHILE GRIDS ARE IN USE!
        /// </summary>
        public static void EmptyAllQueues()
        {
            // Emergency freeze of our generation thread. Will finish the remaining DynGrid.
            DynGridThreadWait.Reset();
            Me.DynGridGenerationThreadHalt = true;

            // Clean out all of the queues. Do not actually process any data.
            while (Me.UrgentQueue.TryDequeue(out _))
                ;
            while (Me.LazyQueue.TryDequeue(out _))
                ;

            // Call this one last to minimize the chance of a race condition leaving this queue with an item. Technically, the second thread
            // could access this after we get here. Hopefully, locks and the processing time for the threads above will ensure that we don't
            // ever leave the thread with a single invalid data DynGrid. We should handle the race anyways.
            while (Me.CallbackQueue.TryDequeue(out _))
                ;
            while (Me.FreeDynGrids.TryDequeue(out _))
                ;

            // Do not reenable the DynGrid generation thread. It will enable itself after a Set signal is sent.
        }

        public void RunDynGridGenerationThread()
        {
            bool dequeueSuccessful;

            while (this.DynGridGenerationThreadRunning)
            {
                dequeueSuccessful = true;
                while (dequeueSuccessful && !this.DynGridGenerationThreadHalt)
                {
                    // Process all of the DynGrids that are currently visible (on the urgent queue).
                    dequeueSuccessful = Me.UrgentQueue.TryDequeue(out DynGridVoxelLoader data);
                    // If there is nothing urgent to do, render some DynGrids offscreen.
                    if (!dequeueSuccessful)
                    {
                        dequeueSuccessful = Me.LazyQueue.TryDequeue(out data);
                    }

                    if (data != null)
                    {
                        //data.Create();
                        // We have done all we can do, send it back to the main thread. Also use safe lock to see if we should drop the data.
                        if (!Me.DynGridGenerationThreadHalt) Me.CallbackQueue.Enqueue(data);
                    }
                }
                DynGridThreadWait.WaitOne();
                Me.DynGridGenerationThreadHalt = false;
            }
        }

        ~GridFactory()
        {
            // If the thread is still running, we should shut it down,
            // otherwise it can prevent the game from exiting correctly.
            if (this.DynGridGenerationThreadRunning)
            {
                // This forces the while loop in the ThreadedWork function to abort.
                this.DynGridGenerationThreadRunning = false;
                // This waits until the thread exits,
                // ensuring any cleanup we do after this is safe. 
                DynGridThreadWait.Set();
                this.DynGridGeneratorAsync.Join();
            }
        }
    }
}
