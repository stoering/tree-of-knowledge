﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Core
{
    public class SaveManager : MonoBehaviour
    {
        public Planet Planet;

        [SerializeField] private string saveFileLocation;
        [SerializeField] private string defaultSaveFile;

        private readonly SaveData SaveGameData = new SaveData();

        public string SaveFileLocation { get => this.saveFileLocation; set => this.saveFileLocation = value; }
        public string DefaultSaveFile { get => this.defaultSaveFile; set => this.defaultSaveFile = value; }

        /// <summary>
        /// Calls save functions on our data structures to convert them to a serializable format.
        /// </summary>
        public void CopyToSaveStructure()
        {
            this.SaveGameData.PlanetSettings = this.Planet.SharedPlanetSettings.SavePlanet();
        }

        /// <summary>
        /// Calls load functions on our data structures to recreate them from the save data.
        /// </summary>
        public void CopyFromSaveStructure()
        {
            this.Planet.SharedPlanetSettings.LoadPlanet(this.SaveGameData.PlanetSettings);
        }

        /// <summary>
        /// Quickly saves the game to the default file.
        /// </summary>
        public void SaveGame()
        {
            SaveToFile(this.DefaultSaveFile);
        }

        /// <summary>
        /// Saves the game to the specified file.
        /// </summary>
        /// <param name="saveFile"></param>
        public void SaveToFile(string saveFile)
        {
            if (saveFile.Contains(".txt") == false)
            {
                saveFile += ".txt";
            }

            CopyToSaveStructure();

            string fullPathSave = Path.Combine(this.saveFileLocation, saveFile);
            FileManager.SaveObject(fullPathSave, this.SaveGameData);
        }

        /// <summary>
        /// Loads the game from the default file.
        /// </summary>
        /// <returns></returns>
        public bool LoadGame()
        {
            return LoadFromFile(this.DefaultSaveFile);
        }

        /// <summary>
        /// Loads the game from the last used file.
        /// </summary>
        /// <param name="saveFile"></param>
        /// <returns></returns>
        public bool LoadFromFile(string saveFile)
        {
            if (saveFile.Contains(".txt") == false)
            {
                saveFile += ".txt";
            }
            string fullPathSave = Path.Combine(this.saveFileLocation, saveFile);
            bool loadSuccessful = true;
            // If the file doesn't exist, just catch and print.
            try
            {
                FileManager.LoadObject(fullPathSave, this.SaveGameData);
                CopyFromSaveStructure();
            }
            catch (FileNotFoundException e)
            {
                Debug.Log(e);
                loadSuccessful = false;
            }

            return loadSuccessful;
        }
    }
}
