﻿using System;

namespace Core
{
    [Serializable]
    public class SaveData
    {
        public SaveablePlanetSettings PlanetSettings;
    }
}
