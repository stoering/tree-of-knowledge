﻿using UnityEngine;

public class ModManager : MonoBehaviour
{
    [SerializeField] private bool debugOn = true;

    public bool DebugOn { get => this.debugOn; set => this.debugOn = value; }

    private void Awake()
    {
        // Set up our mod backend and start the debugger.
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
