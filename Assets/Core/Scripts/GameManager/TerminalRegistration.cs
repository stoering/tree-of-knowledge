﻿using CommandTerminal;
using Interface;
using UnityEngine;

namespace Core
{
    public class TerminalRegistration : MonoBehaviour
    {
        [SerializeField] private SaveManager SaveManager;

        // TODO: Make a slightly better singleton?
        private static TerminalRegistration Me;

        // Start is called before the first frame update
        void Start()
        {
            Me = this;
            if (this.SaveManager == null)
            {
                this.SaveManager = this.GetComponent<SaveManager>();
            }
        }

        [RegisterCommand(Name = "Save", Help = "Saves the game to the default file", MinArgCount = 0, MaxArgCount = 1)]
        static void SaveGame(CommandArg[] args)
        {
            if (args.Length == 0)
            {
                Debug.Log("Saving Game");
                Me.SaveManager.SaveGame();
            }
            else
            {
                string saveFile = args[0].String;
                Debug.Log("Saving Game to " + saveFile);
                Me.SaveManager.SaveToFile(saveFile);
            }
        }

        [RegisterCommand(Name = "Load", Help = "Loads the game data from the default file", MinArgCount = 0, MaxArgCount = 1)]
        static void LoadGame(CommandArg[] args)
        {
            if (args.Length == 0)
            {
                Debug.Log("Loading Game");
                Me.SaveManager.LoadGame();
            }
            else
            {
                string saveFile = args[0].String;
                Debug.Log("Loading Game from " + saveFile);
                Me.SaveManager.LoadFromFile(saveFile);
            }
        }

        [RegisterCommand(Name = "Lua", Help = "Runs whatever the heck I want", MinArgCount = 0, MaxArgCount = 1)]
        static void LearnLua(CommandArg[] args)
        {
            if (args.Length == 0)
            {
                Debug.Log("Passing into Interface");
                ModInterface.DoTestStuff();
            }
            else
            {
                //string saveFile = args[0].String;
                //Debug.Log("Loading Game from " + saveFile);
                //Me.SaveManager.LoadFromFile(saveFile);
            }
        }
    }
}
