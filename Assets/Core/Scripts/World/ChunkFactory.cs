﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using UnityEngine;

namespace Core
{
    /// <summary>
    /// Creates chunks and maintains the free chunk pool. Also maintains the ChunkCreation thread.
    /// </summary>
    public sealed class ChunkFactory
    {
        // This class is a singleton. Use Me to get "this"
        private static readonly Lazy<ChunkFactory> lazy = new Lazy<ChunkFactory>(() => new ChunkFactory());
        private static ChunkFactory Me { get { return lazy.Value; } }

        // Chunk Generation Thread:
        private Thread ChunkGeneratorAsync { get; set; }
        private static AutoResetEvent ChunkThreadWait { get; set; } = new AutoResetEvent(false);
        private bool ChunkGenerationThreadRunning { get; set; } = false;
        private bool ChunkGenerationThreadHalt { get; set; } = false;

        // Our chunks that can be used.
        private ConcurrentQueue<Chunk> FreeChunks { get; set; } = new ConcurrentQueue<Chunk>();
        private Transform FreeChunkParent { get; set; } = null;

        // Internal structure used to generate chunks.
        private ConcurrentQueue<ChunkLoader> FreeChunkData { get; set; } = new ConcurrentQueue<ChunkLoader>();
        // The urgent queue is of critical priority. The lazy queue is only generated when the fast queue is empty.
        private ConcurrentQueue<ChunkLoader> UrgentQueue { get; set; } = new ConcurrentQueue<ChunkLoader>();
        private ConcurrentQueue<ChunkLoader> LazyQueue { get; set; } = new ConcurrentQueue<ChunkLoader>();
        // When we have done all we can do on the second thread, we need to finish setting up the mesh data.
        private ConcurrentQueue<ChunkLoader> CallbackQueue { get; set; } = new ConcurrentQueue<ChunkLoader>();

        // Private and parameterless, guarantees that two can't be created.
        private ChunkFactory()
        {
            this.ChunkGenerationThreadRunning = true;
            this.ChunkGeneratorAsync = new Thread(RunChunkGenerationThread);
            this.ChunkGeneratorAsync.Start();
        }

        /// <summary>
        /// Creates and returns a chunk. The chunk gameobject is nested below the parent transform. The mesh
        ///  will be generated and assigned asymmetrically and automatically. Use chunk.ChunkReady to check progress.
        ///  If this chunk is currently in the camera's field of view, use urgent=true to expedite the generation.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="type"></param>
        /// <param name="faceData"></param>
        /// <param name="bounds"></param>
        /// <param name="urgent"></param>
        /// <returns></returns>
        public static Chunk CreateChunk(ChunkType type, TerrainFace faceData, Vector2[] bounds, bool urgent = false)
        {
            Chunk chunk = CreateInactiveChunk(type, faceData, bounds);

            GenerateChunkMeshes(chunk, urgent);

            //Return the chunk.
            return chunk;
        }

        public static Chunk CreateInactiveChunk(ChunkType type, TerrainFace faceData, Vector2[] bounds)
        {
            Chunk chunk = GetFreeChunk();
            chunk.Initialize(faceData, bounds);

            return chunk;
        }

        public static void GenerateChunkMeshes(Chunk chunk, bool urgent = false)
        {
            if (Me.FreeChunkData.TryDequeue(out ChunkLoader data) == false)
                data = new ChunkLoader();

            if (data == null) throw new ChunkCreationFailureException("Data dequeue fail");

            data.Init(chunk.FaceData, chunk.Bounds, true, true, true);
            // TODO: add data types to chunks when there are more than one. // data.Type = type;

            // Pass a function pointer from our chunk. The rest is automatic from here.
            data.Callback = chunk.SetMesh;

            // Add the chunk to the render queue. 
            if (urgent)
                Me.UrgentQueue.Enqueue(data);
            else
                Me.LazyQueue.Enqueue(data);
            ChunkThreadWait.Set();
        }

        /// <summary>
        /// Returns a chunk that is not currently being rendered. 
        /// </summary>
        /// <returns></returns>
        public static Chunk GetFreeChunk()
        {
            if (Me.FreeChunks.TryDequeue(out Chunk chunk) == false)
            {
                GameObject chunkObject = new GameObject();
                chunkObject.AddComponent<MeshFilter>().mesh = new Mesh();
                chunkObject.AddComponent<MeshRenderer>();
                chunkObject.AddComponent<MeshCollider>();
                chunkObject.AddComponent<Rigidbody>().isKinematic = true;
                chunk = chunkObject.AddComponent<Chunk>();
            }

            return chunk;
        }

        /// <summary>
        /// Sends a chunk back to the free stack so we can reuse it later.
        /// Nests it under the returnChunkParent object if one is specified.
        /// </summary>
        /// <param name="chunk"></param>
        public static void ReturnChunk(Chunk chunk)
        {
            chunk.gameObject.SetActive(false);

            if (Me.FreeChunkParent != null)
            {
                chunk.transform.parent = Me.FreeChunkParent;
            }

            // Add the chunk to our queue.
            Me.FreeChunks.Enqueue(chunk);
        }

        /// <summary>
        /// Set the parent object for our chunks to nest under. If chunks are returned before
        /// this is provided, they will be left in their existing structure.
        /// </summary>
        /// <param name="transform"></param>
        public static void SetReturnChunkParent(Transform transform)
        {
            Me.FreeChunkParent = transform;
        }

        /// <summary>
        /// Take some time off the main thread to create chunks if they are available.
        /// </summary>
        public static void Tick()
        {
            // Check first before declaring anything.
            if (Me.CallbackQueue.TryDequeue(out ChunkLoader data))
            {
                do
                {
                    data.Callback(data);
                    data.Clear();
                    Me.FreeChunkData.Enqueue(data);
                } while (Me.CallbackQueue.TryDequeue(out data));
            }
        }

        /// <summary>
        /// Emergency protocol halts the processing thread and moves all chunk loaders to the free queue.
        /// </summary>
        public static void DropAllProcessingChunks()
        {
            // Emergency freeze of our generation thread. Will finish the remaining chunk.
            ChunkThreadWait.Reset();
            Me.ChunkGenerationThreadHalt = true;
            ChunkLoader data;

            // Clean out all of the queues. Do not actually process any data.
            while (Me.UrgentQueue.TryDequeue(out data))
            {
                Me.FreeChunkData.Enqueue(data);
            }
            while (Me.LazyQueue.TryDequeue(out data))
            {
                Me.FreeChunkData.Enqueue(data);
            }
            // Call this one last to minimize the chance of a race condition leaving this queue with an item. Technically, the second thread
            // could access this after we get here. Hopefully, locks and the processing time for the threads above will ensure that we don't
            // ever leave the thread with a single invalid data chunk. We should handle the race anyways.
            while (Me.CallbackQueue.TryDequeue(out data))
            {
                Me.FreeChunkData.Enqueue(data);
            }

            // Do not reenable the chunk generation thread. It will enable itself after a Set signal is sent.
        }

        public void RunChunkGenerationThread()
        {
            bool dequeueSuccessful;

            while (this.ChunkGenerationThreadRunning)
            {
                dequeueSuccessful = true;
                while (dequeueSuccessful && !this.ChunkGenerationThreadHalt)
                {
                    // Process all of the chunks that are currently visible (on the urgent queue).
                    dequeueSuccessful = Me.UrgentQueue.TryDequeue(out ChunkLoader data);
                    // If there is nothing urgent to do, render some chunks offscreen.
                    if (!dequeueSuccessful)
                    {
                        dequeueSuccessful = Me.LazyQueue.TryDequeue(out data);
                    }

                    if (data != null)
                    {
                        data.Create();
                        // We have done all we can do, send it back to the main thread. Also use safe lock to see if we should drop the data.
                        if (!Me.ChunkGenerationThreadHalt) Me.CallbackQueue.Enqueue(data);
                    }
                }
                ChunkThreadWait.WaitOne();
                Me.ChunkGenerationThreadHalt = false;
            }
        }

        ~ChunkFactory()
        {
            // If the thread is still running, we should shut it down,
            // otherwise it can prevent the game from exiting correctly.
            if (this.ChunkGenerationThreadRunning)
            {
                // This forces the while loop in the ThreadedWork function to abort.
                this.ChunkGenerationThreadRunning = false;
                // This waits until the thread exits,
                // ensuring any cleanup we do after this is safe. 
                ChunkThreadWait.Set();
                this.ChunkGeneratorAsync.Join();
            }
        }
    }
}
