﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class TerrainFace
    {
        public Vector3 localUp;
        public Vector3 axisA;
        public Vector3 axisB;
        public string directionName;
        public List<Material> material;
        public PlanetSettings planetSettings;

        public TerrainFace(Vector3 localUp, string direction, List<Material> material, PlanetSettings planetSettings)
        {
            this.localUp = localUp;
            this.directionName = direction;
            this.material = material;
            this.planetSettings = planetSettings;

            this.axisA = new Vector3(localUp.y, localUp.z, localUp.x);
            this.axisB = Vector3.Cross(localUp, this.axisA);
        }
    }
}
