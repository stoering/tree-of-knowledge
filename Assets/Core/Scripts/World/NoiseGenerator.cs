﻿using BaseLib;
using SimplexNoise;
using UnityEngine;

namespace Core
{
    public static class NoiseGenerator
    {
        /// <summary>
        /// Creates a random simplex noise for the given unit sphere point. 
        /// </summary>
        /// <param name="pointOnUnitSphere"></param>
        /// <returns></returns>
        public static float CalculateUnscaledElevation(Vector3 pointOnUnitSphere)
        {
            return 1 + Noise.Generate(pointOnUnitSphere);
        }
        public static float CalculateUnscaledElevation(Vector3 pointOnUnitSphere, PlanetNoiseSettings settings)
        {
            float elevation = 1;
            float mask = 0;
            float noise = 0;

            foreach (NoiseSettings s in settings.NoiseLayer)
            {
                if (s.enabled)
                {
                    noise = (Noise.Generate(pointOnUnitSphere * s.roughness) * s.strength);

                    if (s.useMask)
                    {
                        noise *= mask;
                    }
                    if (s.addToMask)
                    {
                        mask += noise;
                    }
                    elevation += noise;
                }
            }

            return elevation;
        }
        public static float CalculateUnscaledElevation(Vector3 pointOnUnitSphere, PlanetNoiseSettings settings, int Seed)
        {
            // Convert the seed into a location, then add to the current position. Also potentially mirror each axis.
            Vector3Int signs = new Vector3Int(
                (Seed & (1 << 24)) != 0 ? 1 : -1,
                (Seed & (1 << 25)) != 0 ? 1 : -1,
                (Seed & (1 << 26)) != 0 ? 1 : -1);

            return CalculateUnscaledElevation(new Vector3((signs.x * pointOnUnitSphere.x) + Seed, (signs.y + pointOnUnitSphere.y) + (Seed >> 8), (signs.z + pointOnUnitSphere.z) + (Seed >> 16)), settings);
        }

        public static float GetScaledElevation(float unscaledElevation, float planetRadius)
        {
            float elevation = Mathf.Max(-0.9999f, unscaledElevation);
            elevation = planetRadius * (1 + elevation);
            return elevation;
        }
    }
}
