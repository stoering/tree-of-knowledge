﻿using System;
using System.Collections.Generic;
using BaseLib;
using UnityEngine;

namespace Core
{
    [Serializable]
    public class PlanetSettings
    {
        [SerializeField] public int CurrentPlanetLod = 0;
        [SerializeField] public PlanetLodSettings planetLodSettings;
        [SerializeField] public PlanetNoiseSettings planetNoiseSettings;
        [SerializeField] public PlanetColorSettings planetColorSettings;

        /// <summary>
        /// Converts this PlanetSettings into a SaveablePlanetSettings.
        /// </summary>
        /// <returns></returns>
        public SaveablePlanetSettings SavePlanet()
        {
            SaveablePlanetSettings saveable = new SaveablePlanetSettings()
            {
                PlanetRadius = this.planetLodSettings.PlanetRadius,

                baseSeed = this.planetNoiseSettings.baseSeed,
                ScaledOceanHeight = this.planetNoiseSettings.ScaledOceanHeight,
                NoiseLayer = this.planetNoiseSettings.NoiseLayer
            };

            return saveable;
        }

        /// <summary>
        /// Loads this PlanetSettings from a SaveablePlanetSettings.
        /// </summary>
        /// <param name="saveable"></param>
        public void LoadPlanet(SaveablePlanetSettings saveable)
        {
            this.planetLodSettings.PlanetRadius = saveable.PlanetRadius;

            this.planetNoiseSettings.baseSeed = saveable.baseSeed;
            this.planetNoiseSettings.ScaledOceanHeight = saveable.ScaledOceanHeight;
            this.planetNoiseSettings.NoiseLayer = saveable.NoiseLayer;
        }
    }

    [Serializable]
    public class SaveablePlanetSettings
    {
        // Lod Settings
        public int PlanetRadius = 500;

        // Noise Settings
        public int baseSeed = 0;
        [Range(-100, 100)] public float ScaledOceanHeight = -1;
        public List<NoiseSettings> NoiseLayer = new List<NoiseSettings>();

        // Color Settings
        // None here so far.
    }

    public class Planet : MonoBehaviour
    {
        [SerializeField] public bool Generate = false;
        [SerializeField] public List<Material> faceMaterial = new List<Material>();
        [SerializeField] private int CurrentLod = 0;
        [SerializeField] private Transform ocean;
        [SerializeField] private Transform freeChunkParent;
        [SerializeField] private PlanetSettings sharedPlanetSettings = new PlanetSettings();

        private string[] tempNoiseSettings = new string[1];
        private float tempNoiseBaseSettings = 0;

        [HideInInspector] public bool LodSettingsFoldout;
        [HideInInspector] public bool NoiseSettingsFoldout;
        [HideInInspector] public bool ColorSettingsFoldout;

        public int ChunkResolution { get => this.PlanetLodSettings.ChunkResolution; set => this.PlanetLodSettings.ChunkResolution = value; }

        public TerrainFace[] Faces { get; } = new TerrainFace[6];
        public Chunk[] Chunks { get; } = new Chunk[6];
        public PlanetLodSettings PlanetLodSettings { get => this.SharedPlanetSettings.planetLodSettings; set => this.SharedPlanetSettings.planetLodSettings = value; }
        public PlanetNoiseSettings PlanetNoiseSettings { get => this.SharedPlanetSettings.planetNoiseSettings; set => this.SharedPlanetSettings.planetNoiseSettings = value; }
        public PlanetColorSettings PlanetColorSettings { get => this.SharedPlanetSettings.planetColorSettings; set => this.SharedPlanetSettings.planetColorSettings = value; }
        public PlanetSettings SharedPlanetSettings { get => this.sharedPlanetSettings; set => this.sharedPlanetSettings = value; }

        public Transform Ocean { get => this.ocean; set => this.ocean = value; }
        public Transform FreeChunkParent { get => this.freeChunkParent; set => this.freeChunkParent = value; }
        public List<Material> FaceMaterial { get => this.faceMaterial; }
        public int CurrentLod1 { get => this.CurrentLod; set => this.CurrentLod = value; }

        public void GeneratePlanet()
        {
            Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
            string[] faces = { "Up", "Down", "Left", "Right", "Front", "Back" };
            int planetRadius = this.PlanetLodSettings.PlanetRadius;
            ChunkFactory.SetReturnChunkParent(this.FreeChunkParent);
            ChunkFactory.DropAllProcessingChunks();

            float oceanDepth = planetRadius * (4 + (this.PlanetNoiseSettings.ScaledOceanHeight / 100));

            this.Ocean.localScale = new Vector3(oceanDepth, oceanDepth, oceanDepth);

            for (int i = 0; i < 6; i++)
            {
                if (this.Faces[i] == null)
                {
                    this.Faces[i] = new TerrainFace(directions[i], faces[i], this.FaceMaterial, this.SharedPlanetSettings);
                }
                if (this.Chunks[i] != null)
                {
                    this.Chunks[i].RemoveChunk();
                }
                this.Chunks[i] = ChunkFactory.CreateChunk(ChunkType.WorldChunk, this.Faces[i], new Vector2[] { new Vector2(0, 0), new Vector2(planetRadius, planetRadius) });
                this.Chunks[i].transform.parent = this.transform;
                this.Chunks[i].IsVisible = true;
            }
        }

        /// <summary>
        /// Checks the inputs, then passes the level of detail on to all child chunks.
        /// Throws and exception if the LOD is out of bounds.
        /// </summary>
        /// <param name="Lod"></param>
        public void SetLod(int lod)
        {
            // Check that the lod is valid.
            if (lod < 0 || lod > this.PlanetLodSettings.MaxLodDepth)
            {
                throw new ArgumentOutOfRangeException("Lod depth was " + lod.ToString() + ", expected 0 to " + this.PlanetLodSettings.MaxLodDepth.ToString() + ".");
            }

            this.SharedPlanetSettings.CurrentPlanetLod = lod;
        }

        // Update is called once per frame
        void Update()
        {
            { // TODO: Not needed in the final game, remove once a planet editor exists in-game
                bool generateNeeded = false;
                if (this.tempNoiseSettings.Length != this.PlanetNoiseSettings.NoiseLayer.Count)
                {
                    this.tempNoiseSettings = new string[this.PlanetNoiseSettings.NoiseLayer.Count];
                }
                for (int i = 0; i < this.PlanetNoiseSettings.NoiseLayer.Count; i++)
                {
                    if (JsonUtility.ToJson(this.PlanetNoiseSettings.NoiseLayer[i]).Equals(this.tempNoiseSettings[i]) == false)
                    {
                        generateNeeded = true;
                        this.tempNoiseSettings[i] = JsonUtility.ToJson(this.PlanetNoiseSettings.NoiseLayer[i]);
                    }
                }
                if (Mathf.Abs(this.tempNoiseBaseSettings - (this.PlanetNoiseSettings.ScaledOceanHeight + this.PlanetNoiseSettings.baseSeed)) > 1)
                {
                    this.tempNoiseBaseSettings = this.PlanetNoiseSettings.ScaledOceanHeight + this.PlanetNoiseSettings.baseSeed;
                    generateNeeded = true;
                }
                if (generateNeeded || this.Generate) { this.Generate = false; GeneratePlanet(); };
            }

            ChunkFactory.Tick();

            // Check if any chunks need an update. If so, process that now.
            foreach (Chunk chunk in this.Chunks)
            {
                chunk?.UpdateLod();
            }
        }
    }
}
