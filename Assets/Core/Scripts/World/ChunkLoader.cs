﻿using System;
using UnityEngine;

namespace Core
{
    public enum ChunkType
    {
        WorldChunk,           // Used by spherical planets for LOD.
        MapSectonChunk,       // Subgrid for hexagonal, triangular, and square world.
        VehicleRenderChunk,   // Grid attached to a movable gameobject.
        StructureRenderChunk, // Grid for player made walls and roofs.
    }

    public class ChunkLoader
    {
        /// <summary>
        /// These three arrays make up a mesh.
        /// </summary>
        public Vector3[] Vertices { get; set; }
        public int[] Triangles { get; set; }
        public Vector2[] Uv { get; set; }

        /// <summary>
        /// Our chunk type determines which arrays need to be generated and which can be reused from instance to instance.
        /// Must match the type of the chunk we will return to in this.Callback.
        /// </summary>
        public ChunkType Type { get; set; }

        /// <summary>
        /// The return function of the chunk that we are generating data for.
        /// </summary>
        public Action<ChunkLoader> Callback;

        private TerrainFace FaceData { get; set; }
        private Vector2[] Edges { get; set; }


        /// <summary>
        /// Create (or re-create) a chunk data object of type ChunkType. Arrays will not be created if the array flags are set to null.
        /// </summary>
        /// <param name="resolution"></param>
        /// <param name="updateVerticies"></param>
        /// <param name="updateTriangles"></param>
        /// <param name="updateUvs"></param>
        public void Init(TerrainFace faceData, Vector2[] edges, bool updateVerticies = false, bool updateTriangles = false, bool updateUvs = false)
        {
            // Assign only the arrays that we need to modify on the generation thread.
            if (updateVerticies)
                this.Vertices = new Vector3[faceData.planetSettings.planetLodSettings.ChunkResolution * faceData.planetSettings.planetLodSettings.ChunkResolution];
            if (updateTriangles)
                this.Triangles = new int[(faceData.planetSettings.planetLodSettings.ChunkResolution - 1) * (faceData.planetSettings.planetLodSettings.ChunkResolution - 1) * 6];
            if (updateUvs)
                this.Uv = new Vector2[this.Vertices.Length];


            this.FaceData = faceData;
            this.Edges = edges;
        }

        /// <summary>
        /// Create all of the meshes that this chunk will need.
        /// </summary>
        public void Create()
        {
            int triIndex = 0;
            int chunkSize = this.FaceData.planetSettings.planetLodSettings.ChunkResolution;
            int planetSize = this.FaceData.planetSettings.planetLodSettings.PlanetRadius;

            Vector2 chunkScale = new Vector2(this.Edges[1].x - this.Edges[0].x, this.Edges[1].y - this.Edges[0].y) / (float)(planetSize * (chunkSize - 1));
            Vector2 chunkPosition = new Vector2(this.Edges[0].x / (float)planetSize, this.Edges[0].y / (float)planetSize);

            for (int y = 0; y < chunkSize; y++)
            {
                for (int x = 0; x < chunkSize; x++)
                {
                    int i = x + y * chunkSize;
                    // All of these sizes are normalized. We will multiply by the planet's radius at the very end.
                    Vector2 percent = new Vector2(x * chunkScale.x, y * chunkScale.y);
                    Vector3 pointOnUnitCube = this.FaceData.localUp + (percent.x + chunkPosition.x - .5f) * 2 * this.FaceData.axisA + (percent.y + chunkPosition.y - .5f) * 2 * this.FaceData.axisB;
                    Vector3 pointOnUnitSphere = pointOnUnitCube.normalized;
                    float unscaledElevation = NoiseGenerator.CalculateUnscaledElevation(pointOnUnitSphere, this.FaceData.planetSettings.planetNoiseSettings, this.FaceData.planetSettings.planetNoiseSettings.baseSeed);
                    this.Vertices[i] = pointOnUnitSphere * NoiseGenerator.GetScaledElevation(unscaledElevation, planetSize);

                    this.Uv[i] = new Vector2((x * chunkScale.x) + chunkPosition.x, (y * chunkScale.y) + chunkPosition.y);

                    if (x != chunkSize - 1 && y != chunkSize - 1)
                    {
                        this.Triangles[triIndex] = i;
                        this.Triangles[triIndex + 1] = i + chunkSize + 1;
                        this.Triangles[triIndex + 2] = i + chunkSize;

                        this.Triangles[triIndex + 3] = i;
                        this.Triangles[triIndex + 4] = i + 1;
                        this.Triangles[triIndex + 5] = i + chunkSize + 1;
                        triIndex += 6;
                    }
                }
            }
        }

        /// <summary>
        /// Decouples the chunk data from the mesh that was generated using it.
        /// </summary>
        public void Clear()
        {
            // Since these arrays are now owned by a MeshFilter, we should clean them out.
            this.Vertices = null;
            this.Triangles = null;
            this.Uv = null;
        }
    }
}