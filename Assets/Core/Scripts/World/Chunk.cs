﻿using System;
using UnityEngine;

namespace Core
{
    public enum ViewState
    {
        Visible = 0,
        //NearCameraFrame,
        OutOfFrame
    }

    public class Chunk : MonoBehaviour
    {
        public Chunk ParentChunk { get; set; }
        public Chunk[] ChildChunks { get; set; } = new Chunk[4];
        public Vector2[] Bounds { get; set; }
        public TerrainFace FaceData { get; set; }

        private ViewState chunkVisibility = ViewState.OutOfFrame;

        public bool ChunkReady { get; set; } = false;
        public int ChunkLevel { get; set; } = 0;

        /// <summary>
        /// Activates or deactivates the chunk's mesh renderer.
        /// </summary>
        public bool IsVisible { get => this.GetComponent<MeshRenderer>().enabled; set => this.GetComponent<MeshRenderer>().enabled = value; }

        /// <summary>
        /// Returns the lowest visibility for me and all of my children recursively.
        /// Setting this only sets my value.
        /// </summary>
        public ViewState ChunkVisibility
        {
            get
            {
                ViewState visibility = this.chunkVisibility;
                ViewState chunkVisibility;

                foreach (Chunk chunk in this.ChildChunks)
                {
                    if (chunk)
                    {
                        chunkVisibility = chunk.ChunkVisibility;
                        if (chunkVisibility < visibility)
                        {
                            visibility = chunkVisibility;
                        }
                    }
                }

                return visibility;
            }
            set => this.chunkVisibility = value;
        }

        /// <summary>
        /// Places the chunk in its new location and assigns all relevant fields.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="faceData"></param>
        /// <param name="bounds"></param>
        public void Initialize(TerrainFace faceData, Vector2[] bounds)
        {
            this.name = faceData.directionName + " " + bounds[0].ToString() + " to " + bounds[1].ToString();
            this.FaceData = faceData;
            this.Bounds = bounds;
            this.GetComponent<MeshRenderer>().materials = faceData.material.ToArray();
            this.ChunkVisibility = ViewState.OutOfFrame;
            this.ChunkReady = false;
            this.gameObject.SetActive(true);
        }

        /// <summary>
        /// Callback that creates the mesh filters for the renderer and collider.
        /// </summary>
        /// <param name="data"></param>
        public void SetMesh(ChunkLoader data)
        {
            if (this.gameObject.activeSelf)
            {
                MeshCollider collider = GetComponent<MeshCollider>();
                MeshFilter mesh = GetComponent<MeshFilter>();
                mesh.sharedMesh.Clear();
                mesh.sharedMesh.vertices = data.Vertices;
                mesh.sharedMesh.triangles = data.Triangles;
                mesh.sharedMesh.uv = data.Uv;
                mesh.sharedMesh.RecalculateNormals();

                collider.sharedMesh = mesh.sharedMesh;

                this.ChunkReady = true;
            }
        }

        public void SetChunkEnterVisibility(ViewState visibility)
        {
            if (visibility < this.ChunkVisibility) this.ChunkVisibility = visibility;
        }
        public void SetChunkExitVisibility(ViewState visibility)
        {
            if (visibility >= this.ChunkVisibility) this.ChunkVisibility = visibility + 1;
        }

        public void UpdateLod()
        {
            // The chunk visibility is the most visible state of any child in any level.
            // If a chunk is out of frame, it means that all child layers are out of frame.
            switch (this.ChunkVisibility)
            {
                case ViewState.Visible:
                    //case ViewState.NearCameraFrame:
                    {
                        // Get me relative depth. 0 means I am at the target LOD.
                        int currentLevel = this.FaceData.planetSettings.CurrentPlanetLod - this.ChunkLevel;

                        // I'm to shallow. Activate another level.
                        if (currentLevel > 0)
                        {
                            // We deal with all children together. Assume that if one is missing, all are.
                            if (this.ChildChunks[0] == null)
                            {
                                // We are in frame, create the chunk immediately.
                                CreateChildChunks(true);
                            }
                        }
                        else if (currentLevel == 0)
                        {
                            if (this.ChildChunks[0])
                            {
                                RemoveChildren();
                            }
                        }
                        else // currentLevel < 0
                        {
                            throw new Exception("This chunk should not exist. Check your code, ya dummy!");
                        }
                    }
                    break;
                case ViewState.OutOfFrame:
                    // Double check that we aren't removing the base here.
                    if (this.ChildChunks[0])
                    {
                        RemoveChildren();
                    }
                    break;
                default:
                    break;
            }

            // Loop 2: activate the lowest existing chunk in the LOD structure.
            // Check if all my children exist and contain meshes.
            bool allChildrenReady = true;
            foreach (Chunk chunk in this.ChildChunks)
            {
                if (chunk == null || !chunk.ChunkReady)
                {
                    allChildrenReady = false;
                }
            }
            // If so, activate each of them (or their children).
            if (allChildrenReady)
            {
                foreach (Chunk chunk in this.ChildChunks)
                {
                    chunk.UpdateLod();
                }
                // Explicitly disable me so we don't get duplicate chunks.
                this.IsVisible = false;
            }
            // If not, activate me.
            else
            {
                this.IsVisible = true;
            }
        }

        public void RemoveChildren()
        {
            foreach (Chunk chunk in this.ChildChunks)
            {
                chunk?.RemoveChunk();
            }
            // Remove my references to any children.
            this.ChildChunks = new Chunk[4];
        }

        public void RemoveChunk()
        {
            // Delete each of my children first.
            RemoveChildren();

            // Turn off my visibility.
            this.IsVisible = true;

            // Add me to the factory queue.
            ChunkFactory.ReturnChunk(this);
        }

        /// <summary>
        /// Checks if the children need to be built and create them if they do.
        /// </summary>
        /// <param name="urgent"></param>
        private void CreateChildChunks(bool urgent)
        {
            Vector2 midpoint = new Vector2((this.Bounds[0].x + this.Bounds[1].x) / 2, (this.Bounds[0].y + this.Bounds[1].y) / 2);
            Vector2[][] edges = new Vector2[][] {
                new Vector2[] { new Vector2(this.Bounds[0].x, this.Bounds[0].y), new Vector2(midpoint.x, midpoint.y) },
                new Vector2[] { new Vector2(midpoint.x, this.Bounds[0].y), new Vector2(this.Bounds[1].x, midpoint.y) },
                new Vector2[] { new Vector2(this.Bounds[0].x, midpoint.y), new Vector2(midpoint.x, this.Bounds[1].y) },
                new Vector2[] { new Vector2(midpoint.x, midpoint.y), new Vector2(this.Bounds[1].x, this.Bounds[1].y) }
            };

            for (int i = 0; i < 4; i++)
            {
                if (this.ChildChunks[i] == null)
                {
                    this.ChildChunks[i] = ChunkFactory.CreateChunk(ChunkType.WorldChunk, this.FaceData, edges[i], urgent);
                    this.ChildChunks[i].transform.parent = this.transform;
                    this.ChildChunks[i].ParentChunk = this;
                    this.ChildChunks[i].ChunkLevel = this.ChunkLevel + 1;
                }
            }
        }
    }
}
