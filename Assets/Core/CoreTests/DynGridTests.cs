﻿using System.Collections;
using System.Collections.Generic;
using Core;
using NUnit.Framework;
using NUnit.Framework.Internal;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class DynGridTests
    {
        /// <summary>
        /// The most basic test: Check out grids from the factory and return them.
        /// Simple verification that grids are not being garbage collected upon return.
        /// </summary>
        [Test]
        public void DynGridTestsFactoryCreateAndReturnGrids()
        {
            Randomizer random = Randomizer.CreateRandomizer();
            GameObject daddy = new GameObject();
            int gridCount = random.Next(100);
            GridFactory.SetReturnDynGridParent(daddy.transform);

            List<DynGrid> dynGridsPre = new List<DynGrid>();
            List<DynGrid> dynGridsPost = new List<DynGrid>();

            // Get a few grids from the factory.
            for (int i = 0; i < gridCount; i++)
            {
                dynGridsPre.Add(GridFactory.CreateInactiveDynGrid(Vector3.zero));
                Assert.IsNotNull(dynGridsPre[i]);
            }

            // Return the grids to the factory.
            for (int i = 0; i < gridCount; i++)
            {
                GridFactory.ReturnDynGrid(dynGridsPre[i]);
            }

            // Make sure that all grids are added as children to our parent transform.
            Assert.AreEqual(daddy.transform.childCount, gridCount);

            // Get the grids from the factory again and check that they are the same as before.
            for (int i = 0; i < gridCount; i++)
            {
                dynGridsPost.Add(GridFactory.CreateInactiveDynGrid(Vector3.zero));
                Assert.IsNotNull(dynGridsPost[i]);
                Assert.AreSame(dynGridsPre[i], dynGridsPost[i]);
            }

            // Check out and return a single grid. It should always be the same grid.
            DynGrid singleGrid = GridFactory.CreateInactiveDynGrid(Vector3.zero);
            GridFactory.ReturnDynGrid(singleGrid);
            for (int i = 0; i < gridCount; i++)
            {
                Assert.AreSame(singleGrid, GridFactory.CreateInactiveDynGrid(Vector3.zero));
                GridFactory.ReturnDynGrid(singleGrid);
            }

            // CLEANUP: Return all grids to the factory.
            for (int i = 0; i < gridCount; i++)
            {
                GridFactory.ReturnDynGrid(dynGridsPost[i]);
            }
        }

        /// <summary>
        /// Checks out grids from the factory before and after manual factory garbage collection.
        /// Asserts that the grids no longer exist.
        /// </summary>
        [Test]
        public void DynGridTestsFactoryForceGC()
        {
            Randomizer random = Randomizer.CreateRandomizer();
            int gridCount = random.Next(100);

            List<DynGrid> dynGridsPre = new List<DynGrid>();
            List<DynGrid> dynGridsPost = new List<DynGrid>();

            // Get a few grids from the factory.
            for (int i = 0; i < gridCount; i++)
            {
                dynGridsPre.Add(GridFactory.CreateInactiveDynGrid(Vector3.zero));
                Assert.IsNotNull(dynGridsPre[i]);
            }

            // Return the grids to the factory.
            for (int i = 0; i < gridCount; i++)
            {
                GridFactory.ReturnDynGrid(dynGridsPre[i]);
            }

            // Dump the entire factory into the garbage collecter.
            GridFactory.EmptyAllQueues();

            // Get the grids from the factory again and check that they are different than before.
            for (int i = 0; i < gridCount; i++)
            {
                dynGridsPost.Add(GridFactory.CreateInactiveDynGrid(Vector3.zero));
                Assert.IsNotNull(dynGridsPost[i]);
                Assert.AreNotSame(dynGridsPre[i], dynGridsPost[i]);
            }

            // Return all grids to the factory.
            for (int i = 0; i < gridCount; i++)
            {
                GridFactory.ReturnDynGrid(dynGridsPost[i]);
            }

            // Check out and return a single grid. It should never be the same grid.
            DynGrid singleGrid = GridFactory.CreateInactiveDynGrid(Vector3.zero);
            GridFactory.ReturnDynGrid(singleGrid);

            for (int i = 0; i < gridCount; i++)
            {
                GridFactory.EmptyAllQueues();
                Assert.AreNotSame(singleGrid, GridFactory.CreateInactiveDynGrid(Vector3.zero));
                GridFactory.ReturnDynGrid(singleGrid);
            }

        }
    }
}
