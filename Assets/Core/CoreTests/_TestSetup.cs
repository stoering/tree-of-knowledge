﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using UnityEngine;

namespace Tests
{
    public class AA_TestSetup
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestSetupRandomSeed()
        {
            // Change the seed to fixed if any test fails with a random seed.
            Randomizer.InitialSeed = Random.Range(int.MinValue, int.MaxValue);
            //Randomizer.InitialSeed = 0;
            Debug.Log("Test seed is " + Randomizer.InitialSeed.ToString());
        }
    }
}
