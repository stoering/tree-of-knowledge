﻿using Core;
using NUnit.Framework;
using System;
using System.IO;

namespace Tests
{
    public class SaveManagerTests
    {
        [Serializable]
        struct SaveStructForTest
        {
            public string DataString;
            public int DataInt;
        }

        // Must be identical to the above for these to work interchangeably.
        [Serializable]
        class SaveClassForTest
        {
            public string DataString;
            public int DataInt;
        }

        // A Test behaves as an ordinary method
        [Test]
        public void FileManagerTestsSaveLoadString()
        {
            string saveString = "This string is completely random. Trust me, I made it myself.";
            string saveLocation = Path.Combine("DynamicAssets", "Temp", "SaveLoadTest1.txt");

            FileManager.SaveString(saveString, saveLocation);

            string loadString = FileManager.LoadString(saveLocation);

            Assert.IsTrue(saveString.Equals(loadString));
        }


        [Test]
        public void FileManagerTestsSaveLoadStructure()
        {
            SaveStructForTest saveObject = new SaveStructForTest()
            {
                DataInt = 67,
                DataString = "This string is completely random. Trust me, I made it myself."
            };

            SaveStructForTest loadObject;

            string saveLocation = Path.Combine("DynamicAssets", "Temp", "SaveLoadTest1.txt");

            FileManager.SaveObject(saveLocation, saveObject);

            loadObject = FileManager.LoadObject<SaveStructForTest>(saveLocation);

            Assert.IsTrue(saveObject.Equals(loadObject));
        }

        [Test]
        public void FileManagerTestsSaveLoadStructureByRef()
        {
            SaveStructForTest saveObject = new SaveStructForTest()
            {
                DataInt = 67,
                DataString = "This string is completely random. Trust me, I made it myself."
            };

            SaveStructForTest loadObject = new SaveStructForTest()
            {
                DataInt = -999,
                DataString = "Not the same string."
            };

            string saveLocation = Path.Combine("DynamicAssets", "Temp", "SaveLoadTest1.txt");

            FileManager.SaveObject(saveLocation, saveObject);

            // This function throws a syntax error if the struct is not boxed. On one hand, it kind of nullifies
            // this test. On the other hand, this test was needed for me to figure out how that worked.
            object boxedStruct = loadObject;
            FileManager.LoadObject(saveLocation, boxedStruct);
            loadObject = (SaveStructForTest)boxedStruct;

            Assert.IsTrue(saveObject.Equals(loadObject));
        }

        [Test]
        public void FileManagerTestsSaveLoadObject()
        {
            SaveClassForTest saveObject = new SaveClassForTest()
            {
                DataInt = 67,
                DataString = "This string is completely random. Trust me, I made it myself."
            };

            SaveClassForTest loadObject = new SaveClassForTest()
            {
                DataInt = -999,
                DataString = "Not the same string."
            };

            string saveLocation = Path.Combine("DynamicAssets", "Temp", "SaveLoadTest1.txt");

            FileManager.SaveObject(saveLocation, saveObject);

            FileManager.LoadObject(saveLocation, loadObject);

            Assert.IsTrue(saveObject.DataString.Equals(loadObject.DataString));
            Assert.IsTrue(saveObject.DataInt == loadObject.DataInt);
        }

        [Test]
        public void FileManagerTestsSaveLoadObjectByRef()
        {
            SaveClassForTest saveObject = new SaveClassForTest()
            {
                DataInt = 67,
                DataString = "This string is completely random. Trust me, I made it myself."
            };

            SaveClassForTest loadObject;

            string saveLocation = Path.Combine("DynamicAssets", "Temp", "SaveLoadTest1.txt");

            FileManager.SaveObject(saveLocation, saveObject);

            loadObject = FileManager.LoadObject<SaveClassForTest>(saveLocation);

            Assert.IsTrue(saveObject.DataString.Equals(loadObject.DataString));
            Assert.IsTrue(saveObject.DataInt == loadObject.DataInt);
        }
    }
}
